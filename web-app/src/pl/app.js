const express = require('express')
const mysql = require("mysql")
const expressHandlebars = require("express-handlebars")

const db = mysql.createConnection({
  host: "db",
  user: "root",
  password: "abc123",
  database: "myDB"
})

const app = express()

app.set("views","src/pl/views")

app.engine("hbs",expressHandlebars({
  defaultLayout :"main.hbs"
}))
app.get('/', function(request, response){
  response.render("home.hbs")
})

app.listen(8080, function(){
  console.log("Web application listening on port 8080.")
})